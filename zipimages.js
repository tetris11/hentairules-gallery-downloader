
class ZipImages {

    constructor( title, image_array, thumb_url = null )
    {
	// if null, grab thumb from center
	thumb_url = thumb_url || image_array[Math.floor(image_array.length / 2)].replace('-xx.','-th.')

	this.logbox = new LogBox( title, thumb_url )
	
	this.zip = new JSZip();
        this.total_imgs = image_array.length;
	this.counter = 0;

        for (var i=0; i < image_array.length; i++)
        {
            var num = i + 1;    
	    this.__addImage( num, image_array[i] );
        }
	this.__complete( title );
    }

    urlToPromise(url)
    {
	var that = this;
        return new Promise(function(resolve, reject) {
            JSZipUtils.getBinaryContent(url, function (err, data) {
                if(err) {
                    reject(err);
                } else {
		    that.logbox.setBody("["+ (++that.counter) + " / " + that.total_imgs + "]");
                    resolve(data);
                }
            });
        });
    }


    __addImage( number, image_url)
    {
        var str = "" + number,
            pad = "000",
            num = pad.substring(0, pad.length - str.length) + str;

        var ext_split = image_url.split('.'),
            ext = ext_split[ext_split.length - 1];

        var numname = num + "." + ext;

	//console.log("Processing", image_url, numname);

	var that = this;
	that.zip.file(
	    numname,
	    that.urlToPromise(image_url),
	    {binary:true}
	);
    }

    __complete(title)
    {
        var zipname = this.__zipname(title);
        console.log(zipname);

	var that = this;
	
        that.zip.generateAsync({type:"blob"}).then(
	    function(content){	
		var a = document.createElement('a')
		a.href = window.URL.createObjectURL(content);
		a.innerHTML = title
		a.download = zipname;
		a.style.color = "orange"

		that.logbox.setTitle("");
		that.logbox.title.appendChild(a);
	    }
	);
    }

    __zipname(title){
        var name = title.replace(/[^A-Za-z+_0-9.]+/g,"_");
	//console.log(title, name)
        return name  + ".zip";
    }
}
