# HentaiRules Gallery Downloader #

Downloads all gallery archives on a given page as so:

### Installation ###

 * Download and unpack files into a directory
 * Go to about:debugging in Firefox
 * Load Temporary Addon...  -> select any file in the unpacked directory
 * Once loaded, go to https://www.hentairules.net

### Usage ###

![time.png](https://bitbucket.org/repo/xKeLre/images/1029979586-time.png)


### Why is this not accessible from the Firefox Addons? ###

It violates Mozilla's fair terms of use. See [here](https://www.mozilla.org/en-US/about/legal/acceptable-use/)