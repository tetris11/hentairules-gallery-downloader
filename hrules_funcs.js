

var ProcessMainPage = {

    init: function(){
	var urls  = ProcessMainPage.listGalleries();

	for (var g=0; g < urls.length; g++)
	{
	    ProcessMainPage.processGalleryUrl( urls[g] );

	    // remove after debugging
	    //return 0;
	}
    },

    listGalleries: function(){
	var urls = getClassName(document, "more-link").map(
	    function(a){return a.href;}
	);
	console.log("urls = ", urls)

	return urls;
    },


    // Handle packs
    __processMultipleGalleryUrl: function(document){

	function getURLs2(){ // possibly a complete replacement for getURLs1...
	    var a_urls = getTagName(document, 'a').filter(
		function(a){
		    return (a.innerHTML.length > 0) && (a.href.indexOf('/galleries')!==-1);
		}
	    )
	    return a_urls.map(function(a){return a.href;});
	}


	function getURLs1(){
	
	    var multiple_urls_p_container = getTagName(document, "p").filter(
		function(p){
		    return p.innerHTML.toLowerCase().find("complete pictures")!==-1;
		}
	    )[0] || null;

	    if (multiple_urls_p_container === null){ return null;}

	    
	    
	    var urls_container = Array.prototype.slice.call(multiple_urls_p_container.children).filter(
		function(a){ return a.tagName === "STRONG";}
	    )[0];

	    var urls = [];
	    
	    function aref_search(e){
		if (e.tagName === "A"){
		    urls.push( e.href );
		    return 0;
		}
		
		if (e.tagName === "STRONG"){
		    for (var r=0; r < e.children.length; r++){
			aref_search( e.children[r] );
		    }
		}
	    }
	    
	    aref_search(urls_container);
	    return urls;
	}

	var urls = getURLs1()
	if (urls === null){
	    console.log("trying getURLs2");
	    urls = getURLs2()
	}
	
	console.log("urls=", urls);
	for (var u=0; u < urls.length; u++){
	    ProcessGallery.galleryGetImageUrls( urls[u] );
	}

    },


    processGalleryUrl: function(galleryIndex){

	console.log("galleryIndex=", galleryIndex);

	getPage(galleryIndex, function(document){

	    // recurse all tags within an <a> element. These cannot nest another <a> elem
	    function recur(elem){
		if (elem.children.length === 0
		    && elem.innerHTML.startsWith("Open the Complete Pictures")){
		    return true;
		}
		
		for (var ch = 0; ch < elem.children.length; ch++){
		    // return only on a hit
		    if (recur( elem.children[ch] )){
			return true;
		    }
		}
		return false;
	    }

	    var gallery_url = (function(){
		var g_urls = getTagName(document, "a");

		for (var g = 0; g < g_urls.length; g++)
		{
		    var aref = g_urls[g];
		    
		    if ( recur(aref) ){
			return aref.href;
		    }
		}
		return null
	    })();

	    console.log("gallery_url=", gallery_url);

	    if (gallery_url != null){
		ProcessGallery.galleryGetImageUrls( gallery_url );
		return 0;
	    }

	    // Otherwise it's a pack
	    ProcessMainPage.__processMultipleGalleryUrl(document);
	});
    }
}






var ProcessOldGallery = {

    __getImg: function( href, nextcallback = null )
    {
	console.log("initial", href);
	
	getFramePage(href, function(doc_frames){
		    
	    var next_url = getTagName( doc_frames, "FRAME")
		.filter(function(a){return a.name == "main";})
		.map(   function(a){return a.src;})[0];
		    
	    console.log("next_url", next_url);

	    getPage( next_url, function(doc_img){
		console.log("third");
		
		var img = getTagName( doc_img, 'img')
		    .filter( function(a){return a.src.length > 0;} )
		    .map(    function(a){return a.src;});
				 
		console.log(img);
		if (nextcallback !== null){ nextcallback(); }
	    });
	);

	
    },

    init: function(document)
    {
	console.log("yar");
	
	var title = ("" + window.location + "").split('/'),
	    title = title[title.length - 2];

	console.log("title=", title);

	var img_hrefs = getTagName(document, 'img').filter( function(a){ return a.src.indexOf("th_")!==-1;} ).map( function(a){ return a.parentNode.href;});
	console.log(img_hrefs)

	var ultimate_callback = function(){};

	function run( num, ref, cb){
	    console.log(num, ref)
	    ProcessOldGallery.__getImg( ref);
	    cb();
	}

	
	for (var ih = img_hrefs.length - 1; ih >= 0; --ih){

	    var img_ref = img_hrefs[ih];
	    
	    var newcb = run.bind(null, ih, img_ref, ultimate_callback );
	    ultimate_callback = newcb;
	}

	//console.log(ultimate_callback);
	ultimate_callback();
    }
}










var ProcessGallery = {

    galleryGetImageUrls: function(galleryFirstPage, title = null, total_imgs = [] ){

	getPage(galleryFirstPage,
		function(document){

		    console.log("ere")

		    if (title === null){
			title = getTagName(document, "title")[0].innerHTML.split(" | ")[0].trim()

			// old archive, process seperately
			if (title.startsWith("Pictures Galleries")){
			    console.log("Old archive detected")

			    // pass doc because they have entirety of images, no subsequent page loads needed
			    ProcessOldGallery.init(document);
			    return 0;
			}
			//console.log("title", title)			
		    }

		    var gallery = getTagName(document, 'link').filter(
			function(a){
			    return a.title=="Home";
			}
		    )[0].href || null;

		    if (gallery === null){
			console.log("cannot process", title, galleryFirstPage);
			return -1;
		    }
		    
		    
		    var nextPage = ProcessGallery.nextGalleryPage(document, gallery);
		    //console.log("next page=", nextPage);

		    
		    var imgs_on_current_page = getTagName(document, "IMG").filter(
			function(a){
				return a.className === "thumbnail";
			    }
		    );

	    
		    var img_urls = imgs_on_current_page.map(function(a){
			a = a.src;
			var ext = a.split('.');
			ext = ext[ext.length - 1]
		
			var url = a.split('-th.')[0] + "-xx." + ext;
			var spl = url.split(".net/");
			var last = spl[1].split("_data/")[1];
			
			var newurl = gallery + "_data/" + last;

//			console.log(a, ext, url, spl, last ,newurl)
			
			return newurl;
		    });
		    console.log(img_urls);

		    total_imgs.push.apply( total_imgs, img_urls );

		    if (nextPage != false){
			ProcessGallery.galleryGetImageUrls( nextPage, title, total_imgs );
		    }
		    else {
			(new ZipImages(title, total_imgs));
		    }
		    return 0;
		}
	       );		 
    },
    
    nextGalleryPage: function(document, gallery){

	var nextPageUrl = getClassName(document, "navPrevNext").filter(
	    function(a){
		if (a.children.length>0){
		    return a.children[0].innerHTML == "Next";
		}
		return false;
	    }
	);

	if (nextPageUrl.length === 0){
	    return false;
	}

	var href = nextPageUrl[0].children[0].href;
	var spl = href.split(".net/");
	var last = spl[1].split("index.php")[1]

	return gallery + "index.php" + last;
    }
}



window.onload = function(){
    document.body.style.border = "1px solid red";
    LogArea.init();


    // Run once per page  
    var process = function(){
	ProcessMainPage.init();
    };

    var name = "Download All Archives";
    
    var location = "" + window.location,
	loc_ymd = location.split('/');

  
    try {
	// Already on an archive?
	var year = Number( loc_ymd[3] ) || null, 
	    mon  = Number( loc_ymd[4] ) || null,
	    day  = Number( loc_ymd[5] ) || null;

	console.log(name);

	if (year === null){
	    throw Error();
	}

	name = "Download Current Archive";
	process = function(){
	    ProcessMainPage.processGalleryUrl(location);
	};

    } catch (e){

	console.log(name);

	// Nope, maybe a gallery?
	var gallery = loc_ymd[3].startsWith("gallery");
	if (gallery)
	{
	    console.log(name);
	    
	    var first_url = location;
	    
	    var notfirst = loc_ymd[ loc_ymd.length - 1 ].startsWith("start-");
	    if (notfirst)
	    {
		loc_ymd[ loc_ymd.length - 1 ] = "";
		first_url = loc_ymd.join('/');
	    }
	    
	    name = "Download Gallery";
	    process = function(){
		ProcessGallery.galleryGetImageUrls( first_url );
	    };
   	}
    }

    
    var button = document.createElement('button');
    button.innerHTML = name;

    button.style.position = "absolute";
    button.style.right    = "10px";
    button.style.top      = "10px";

    button.onclick = function(){
	process();
	button.parentNode.removeChild(button);	
    };


    document.body.appendChild( button );
}
