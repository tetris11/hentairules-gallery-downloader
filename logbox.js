
var LogArea = {

    box : null,

    init: function(){
	var box = document.createElement('div')
	box.style.position = "absolute"
	box.style.left = "0px";
	box.style.top = "0px";
	box.style.zIndex = 10000;

	document.body.appendChild( box );
	LogArea.box = box;	
    }
}


class LogBox {

    constructor( a_title, a_thumb = null ){
	
	var box = document.createElement('div');
	box.style.position     = "relative";
	box.style.width        = "32em";
	box.style.borderRadius = "10px";
	box.style.border       = "2px solid grey";
	box.style.background   = "blue";
	box.style.margin       = "2px";
	box.style.height       = "5.4em";


	var title = document.createElement('h5');
	title.style.marginTop   = "-4.5em";
	title.style.marginLeft  = "4em";

	var body = document.createElement('p');
	body.style.marginLeft= "27em"
//	body.style.marginTop = "-10px"
//	body.style.width     = "200px"
	body.style.fontStyle = "oblique"

	if (a_thumb !== null){

	    var thumbnail = document.createElement('img');
	    thumbnail.src              = a_thumb
	    thumbnail.style.width      = "3em"
	    thumbnail.style.marginLeft = "0.5em";
	    thumbnail.style.marginTop  = "0.5em";

	    box.appendChild( thumbnail );
	}

	box.appendChild( title );
	box.appendChild( body  );

	LogArea.box.appendChild( box );

	this.title = title;
	this.body  = body;
	this.setTitle( a_title );
    }

    setTitle( title ){
	this.title.innerHTML = title;
    }

    setBody( body ){
	this.body.innerHTML = body;
    }
}
