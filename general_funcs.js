
function getClassName(document, text){
    return Array.prototype.slice.call( document.getElementsByClassName(text) );
}

function getTagName(document, text){
    return Array.prototype.slice.call( document.getElementsByTagName(text) );
}


function getPage(page_url, onfinish){

    var page = new XMLHttpRequest();
    page.open("GET", page_url)
    page.onload = function(){
	if (page.readyState === 4 ){

	    var DOMtext = page.response;
	    var template = document.createElement('div');
	    template.innerHTML = DOMtext;

//	    console.log("got response", template, onfinish.name)
//	    console.log("a=",template.getElementsByTagName('a'))
	    onfinish(template);
	}
    };
    page.send();
//    console.log("sending xhr", page_url);
}


function getFramePage(page_url, onfinish){

    var page = new XMLHttpRequest();
    page.open("GET", page_url)
    page.onload = function(){
	if (page.readyState === 4 ){

	    var DOMtext = page.response;
	    console.log(DOMtext);
	    var template = document.createElement('html');
	    template.innerHTML = DOMtext;

//	    console.log("got response", template, onfinish.name)
//	    console.log("a=",template.getElementsByTagName('a'))
	    onfinish(template);
	}
    };
    page.send();
//    console.log("sending xhr", page_url);
}
